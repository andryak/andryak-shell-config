# Defined in - @ line 2
function fish_greeting
	set -l quote1 "\
A foolish consistency is the hobgoblin of little minds \
[Ralph Waldo Emerson]"

    set -l quote2 "\
Contrariwise, if it was so, it might be; \
and if it were so, it would be; but as it isn't, it ain't. That's logic. \
[Lewis Carroll]"

    set -l quote3 "\
`But I don't want to go among mad people`, said Alice. \
`Oh, you can't help that`, said the cat. `We're all mad here`. \
[Lewis Carroll]"

    set -l quote4 "\
Sally sells sea shells on the sea shore. \
The shells she sells are sea shells, I am sure!"

    random choice $quote1 $quote2 $quote3 $quote4
end
