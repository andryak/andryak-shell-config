# Defined in - @ line 1
function funcdelete
	set -l FISH_CONFIG "~/.config/fish/functions"
    for funcname in $argv
        echo "deleting function $funcname"
        functions --erase $funcname
        rm -f $FISH_CONFIG/$funcname.fish
    end
end
